package main

import "fmt"

func main() {
	numbers := []int{2, 4, 5, 2, 34, 3}
	sum := sumUp(2, 4, 5, 2, 34, 3)

	anotherSum := sumUp(1, numbers...)

	fmt.Println((sum))
	fmt.Println(anotherSum)

}

func sumUp(startingValue int, numbers ...int) int {
	sum := 0

	for _, val := range numbers {
		sum += val
	}

	return sum
}
