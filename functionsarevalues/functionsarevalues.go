package functionsarevalues

import "fmt"

type transformer func(val int) int

func main() {
	numbers := []int{1, 2, 3, 4}
	moreNumbers := []int{5, 3, 5, 3}

	tFn1 := getTransformerFunction(&numbers)
	tFn2 := getTransformerFunction(&moreNumbers)

	doubled := transformNumbers(&numbers, tFn1)
	tripled := transformNumbers(&numbers, tFn2)

	fmt.Println(doubled)
	fmt.Println(tripled)

	doubled = transformNumbers(&numbers, tFn1)
	tripled = transformNumbers(&numbers, tFn2)

	fmt.Println(doubled)
	fmt.Println(tripled)
}

func transformNumbers(numbers *[]int, transform transformer) []int {
	dNumbers := []int{}
	for _, value := range *numbers {
		dNumbers = append(dNumbers, transform(value))
	}
	return dNumbers
}

func double(number int) int {
	return number * 2
}

func triple(number int) int {
	return number * 3
}

func getTransformerFunction(numbers *[]int) transformer {
	if (*numbers)[0] == 1 {
		return double
	} else {
		return triple
	}
}
